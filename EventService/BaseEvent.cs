﻿///////////////////////////////////////////////////////////////////////////////
// 
// Copyright © 2012 Michael Delz.  
//
///////////////////////////////////////////////////////////////////////////////


namespace DelzEventService {
    using System;
    using System.Xml.Serialization;


    /// <summary>
    ///     The base event implement the functionality, which is required by IDelzEvent.
    ///     <para>It is serializeable.</para>
    /// </summary>
    [Serializable]
    public class BaseEvent : IDelzEvent {

        private readonly DateTime _firedAt = DateTime.Now;
        private bool _isConsumed;


        #region DelzEvent Member

        /// <summary>
        ///     The creation time of this event.
        /// </summary>
        public DateTime FiredAt {
            get { return _firedAt; }
        }

        /// <summary>
        ///     A flag, which tells if this event is already consumed.
        /// </summary>
        [XmlIgnore]
        public bool IsConsumed {
            get { return _isConsumed; }
        }

        /// <summary>
        ///     Consumes this event. Other methods can check this and can stop handling the event.
        /// </summary>
        public void Consume() {
            _isConsumed = true;
        }

        /// <summary>
        ///     Checks if that event is a dublication of this event.
        /// </summary>
        /// <param name="that">The event to compare with.</param>
        /// <returns>This equals that</returns>
        public bool IsDuplicateOf(IDelzEvent that) {
            return Equals(that);
        }

        #endregion
    }
}