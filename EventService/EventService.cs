﻿///////////////////////////////////////////////////////////////////////////////
// 
// Copyright © 2012 Michael Delz.  
//
///////////////////////////////////////////////////////////////////////////////


namespace DelzEventService {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;


    /// <summary>
    ///     EventService distribute an event globally under all subscribers.
    /// </summary>
    public class EventService {

        private readonly Queue<IDelzEvent> _queue = new Queue<IDelzEvent>(20);
        private readonly Dictionary<Type, List<Subscriber>> _subscriber = new Dictionary<Type, List<Subscriber>>();
        private bool _open = true;

        /// <summary>
        ///     Das gegebene Objekt unterschreibt beim EventService und wird von nun an über abgefeurte Events informiert, die es interessieren.
        /// </summary>
        /// <param name="subscriber">Unterzeichnendes Objekt</param>
        public void Subscribe(Object subscriber) {
            if (subscriber == null) {
                return;
            }

            Subscribe(subscriber.GetType(), subscriber);
        }

        private void Subscribe(Type type, Object subscriber) {
            if (type == null || type == typeof (Object)) {
                return;
            }

            // Get the methods from the type
            MethodInfo[] methods = type.GetMethods();

            foreach (MethodInfo method in methods) {
                // Get the attributes of the method
                object[] attr = method.GetCustomAttributes(typeof (EventListener), true);
                // EventListener methods have an EventListener-Attribute
                if (attr.Length != 1) {
                    continue;
                }


                var parameters = method.GetParameters();
                // EventListener methods have one parameter
                if (parameters.Length != 1) {
                    continue;
                }

                var paraType = parameters[0].ParameterType;
                // that parameter is assignable to DelzEvent
                if (!typeof (IDelzEvent).IsAssignableFrom(paraType)) {
                    continue;
                }

                Subscribe(paraType, subscriber, method, ((EventListener) attr[0]).AsThread);
            }
        }

        private void Subscribe(Type eventType, Object subscriber, MethodInfo method, bool asThread) {
            var sub = new Subscriber(subscriber, method, asThread);

            // maps events to the listeners
            List<Subscriber> subscriberList;
            if (!_subscriber.TryGetValue(eventType, out subscriberList)) {
                subscriberList = new List<Subscriber>();
                _subscriber.Add(eventType, subscriberList);
            }

            subscriberList.Add(sub);
        }

        /// <summary>
        ///     Fires an event. All subscriber methods which have the given event as parameter will be invoked.
        /// </summary>
        /// <param name="delzEvent">The fired event</param>
        public void Fire(IDelzEvent delzEvent) {
            if (delzEvent == null) {
                return;
            }

            var redudant = _queue.Any(delzEvent.IsDuplicateOf);

            if (!redudant) {
                _queue.Enqueue(delzEvent);
            }

            if (_open) {
                _open = false;

                while (_queue.Count > 0) {
                    FireEvents();
                }

                _open = true;
            }
        }

        private void FireEvents() {
            while (_queue.Count > 0) {
                var evt = _queue.Dequeue();

                List<Subscriber> subscriber;
                if (!_subscriber.TryGetValue(evt.GetType(), out subscriber)) {
                    continue;
                }
                foreach (Subscriber sub in subscriber) {
                    sub.OnEvent(evt);
                }
            }
        }

    }
}