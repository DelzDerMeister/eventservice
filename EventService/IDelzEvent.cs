﻿///////////////////////////////////////////////////////////////////////////////
// 
// Copyright © 2012 Michael Delz.  
//
///////////////////////////////////////////////////////////////////////////////


namespace DelzEventService {
    using System;


    /// <summary>
    ///     An interface, which all events fired by EventService have to implement.
    /// </summary>
    public interface IDelzEvent {

        /// <summary>
        ///     The creation time of this event.
        /// </summary>
        DateTime FiredAt { get; }

        /// <summary>
        ///     A flag, which tells if this event is already consumed.
        /// </summary>
        bool IsConsumed { get; }

        /// <summary>
        ///     Consumes this event. Other methods can check this and can stop handling the event.
        /// </summary>
        void Consume();

        /// <summary>
        ///     Checks if that event is a dublication of this event.
        /// </summary>
        /// <param name="that">The event to compare with.</param>
        /// <returns>This equals that</returns>
        bool IsDuplicateOf(IDelzEvent that);

    }
}