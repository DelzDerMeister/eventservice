﻿///////////////////////////////////////////////////////////////////////////////
// 
// Copyright © 2012 Michael Delz.  
//
///////////////////////////////////////////////////////////////////////////////


namespace DelzEventService {
    using System;


    /// <summary>
    ///     EventListener annotates a method, which listens to a specific event.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method |
                    AttributeTargets.Delegate,
        AllowMultiple = false)
    ]
    public sealed class EventListener : Attribute {

        private readonly bool _asThread;

        /// <summary>
        ///     Creates a new instance of the EventListener attribute.
        /// </summary>
        public EventListener()
            : this(false) {}

        /// <summary>
        ///     Creates a new instance of the EventListener attribute.
        /// </summary>
        /// <param name="asThread">Tells the EventService, that the annotated method shall invoked as thread.</param>
        public EventListener(bool asThread) {
            _asThread = asThread;
        }

        /// <summary>
        ///     Tells the EventService, if the annotated method should be invoked in a new thread.
        /// </summary>
        public bool AsThread {
            get { return _asThread; }
        }

    }
}