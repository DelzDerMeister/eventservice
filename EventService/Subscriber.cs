﻿///////////////////////////////////////////////////////////////////////////////
// 
// Copyright © 2012 Michael Delz.  
//
///////////////////////////////////////////////////////////////////////////////


namespace DelzEventService {
    using System;
    using System.Reflection;
    using System.Threading;


    internal class Subscriber {

        private readonly bool _isThreaded;
        private readonly MethodInfo _method;
        private readonly Object _subscriber;

        public Subscriber(Object subscriber, MethodInfo method) {
            _method = method;
            _subscriber = subscriber;
        }

        public Subscriber(Object subscriber, MethodInfo method, bool asThread) {
            _method = method;
            _subscriber = subscriber;
            _isThreaded = asThread;
        }

        public void OnEvent<TE>(TE e) where TE : IDelzEvent {
            if (_isThreaded) {
                ThreadStart thread = () => _method.Invoke(_subscriber, new object[] {e});
                new Thread(thread).Start();
            } else {
                _method.Invoke(_subscriber, new object[] {e});
            }
        }

    }
}